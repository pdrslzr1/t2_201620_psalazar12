package mundo;
import  java.util.Date;

/**
 * Created by pedrosalazar on 8/11/16.
 */
public class Trabajo extends Evento {



    public enum TipoEventoTrabajo {

        JUNTA,
        ALMUERZO,
        CENA,
        COCTEL,
        PRESENTACION,
        OTRO,
    }

    protected TipoEventoTrabajo tipo;

    public Trabajo (Date pFecha,String pLugar,TipoEventoTrabajo pTipo, Boolean pObligatorio, boolean pFormal)
    {
        super(pFecha,pLugar,pObligatorio,pFormal);
        tipo = pTipo;
    }



}
