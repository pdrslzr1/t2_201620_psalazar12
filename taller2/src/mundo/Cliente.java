package mundo;

import java.util.Date;

/**
 * Created by pedrosalazar on 8/11/16.
 */
public class Cliente extends Trabajo
{
    private String empresa;

    public Cliente(Date pFecha, String pLugar, TipoEventoTrabajo pTipo, boolean pObligatorio, boolean pFormal, String pEmpresa)
    {
        super(pFecha,pLugar,pTipo,pObligatorio,pFormal);
        empresa = pEmpresa;
    }
    public String conv (Boolean b)
    {
        if (!b) return "No";
        else
            return "Si";
    }

    public String toString()
    {
        return "Evento con CLIENTES: \n"
                +"FECHA: " + this.fecha
                +"\nLUGAR: " + this.lugar
                +"\nTIPO EVENTO: " + this.tipo
                +"\nOBLIGATORIO: " + conv(this.obligatorio)
                +"\nFORMAL: "+ conv(this.formal);
    }
}
